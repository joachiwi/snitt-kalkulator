#script som du kan legge til karakterene dine og regne ut snittet ditt.
#scriptet tar også høyde for at alle fagene gir samme poengsum.

$karakter = 0
$antkar = 0
$snitt = 0
$svar = "X"


Write-Output `n 'Forst, legg til alle karakterene din og deretter trykk "Se karaktersnitt"' `n

Write-Output "1 - Legg til karakterer"
Write-Output "2 - Se karaktersnitt"
Write-Output "3 - Reset karakterer"
Write-Output "9 - Avslutt"

$ans = Read-Host "Velg et alternativ"
while ($ans -ne 9){
    switch ($ans){
        1{Write-Output "Legg til karakterer" `n
            $ans2 = Read-Host "Skriv en karakterer (i bokstav) og trykk enter for hver av dem (avslutt med Q):" `n
            while($ans2 -ne "Q"){
                switch ($ans2){
                    A{Write-Output "A er lagt til" 
                    $karakter += 5
                    $antkar += 1
                    }
                    B{Write-Output "B er lagt til" 
                    $karakter += 4
                    $antkar += 1
                    }
                    C{Write-Output "C er lagt til" 
                    $karakter += 3
                    $antkar += 1
                    }
                    D{Write-Output "D er lagt til" 
                    $karakter += 2
                    $antkar += 1
                    }
                    E{Write-Output "E er lagt til" 
                    $karakter += 1
                    $antkar += 1
                    }
                    F{Write-Output "F er lagt til" 
                    $antkar += 1
                    }
                    Q{`n}
                    default{Write-Output "$ans2 mo vere en bokstav!"}
                }
                $ans2 = Read-Host 
            }
        }
        2{ if ($antkar -gt 0){
            $snitt = $karakter / $antkar
        } 
        if($snitt -ge 4.5){
            $svar = "A"
        }elseif ($snitt -ge 3.5 -And $snitt -lt 4.5) {
            $svar = "B"
        }elseif ($snitt -ge 2.5 -And $snitt -lt 3.5) {
            $svar = "C"
        }elseif ($snitt -ge 1.5 -And $snitt -lt 2.5) {
            $svar = "D"
        }elseif ($snitt -ge 0.5 -And $snitt -lt 1.5) {
            $svar = "E"
        }else {
            $svar = "F"
        }
        
        Write-Output "snittet ditt er: $snitt som er ca en: $svar"
        }
        3{Write-Output "Resetter karakterer"
        $karakter = 0
        $antkar = 0
        $snitt = 0
        $svar = "X"
        }
        9{Write-Output "Avslutter programmet"}
        deafault{Write-Output "$ans ???"}
    }
    Write-Output "1 - Legg til karakterer"
    Write-Output "2 - Se karaktersnitt"
    Write-Output "3 - Reset karakterer"
    Write-Output "9 - Avslutt" 
    
    $ans = Read-Host "Velg et alternativ" 

    
}